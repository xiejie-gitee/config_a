//
//  ObjcConfig_a.h
//  Objc
//
//  Author:     xiejie
//  Email:      632663019@qq.com
//  CreateDate: 2021/11/17
//
    

#ifndef ObjcConfig_a_h
#define ObjcConfig_a_h

#mark 可配置
// 颜色
#import "UIColor+Style.h"
// 字体
#import "UIFont+Style.h"
// 网络
#import "AFNManager.h"

//---------------------------------------
#mark 依赖库 不可修改
// 分类
#import "ObjcCategory.h"

//图片缓存
#import "SDWebImage.h"

#endif /* ObjcConfig_a_h */
