//
//  UIFont+Style.h
//  Objc
//
//  Author:     xiejie
//  Email:      632663019@qq.com
//  CreateDate: 2021/11/17
//
    

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (Style)
+(UIFont *)fontSize:(float)size;
+(UIFont *)fontBoldSize:(float)size;

@end

NS_ASSUME_NONNULL_END
