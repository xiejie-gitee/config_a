//
//  UIColor+Style.m
//  Objc
//
//  Author:     xiejie
//  Email:      632663019@qq.com
//  CreateDate: 2021/11/17
//
    

#import "UIColor+Style.h"

@implementation UIColor (Style)
+(UIColor *)color_text{
    return [UIColor orangeColor];
}
+(UIColor *)color_theme{
    return [UIColor grayColor];
}
+(UIColor *)color_title{
    return [UIColor grayColor];
}
+(UIColor *)color_splitLine{
    return [UIColor grayColor];
}
+(UIColor *)color_background{
    return [UIColor grayColor];
}
+(UIColor *)color_option_highLight{
    return [UIColor grayColor];
}
+(UIColor *)color_option_normal{
    return [UIColor grayColor];
}
+(UIColor *)color_shadow{
    return [UIColor grayColor];
}
+(UIColor *)color_border{
    return [UIColor grayColor];
}

@end
