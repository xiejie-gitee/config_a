//
//  UIColor+Style.h
//  Objc
//
//  Author:     xiejie
//  Email:      632663019@qq.com
//  CreateDate: 2021/11/17
//
    

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Style)

/*
 app项目基本配色方案如下，如果缺少相应的主题的颜色，请自行添加
 
 **/

/// 主题颜色、导航栏
@property(class, nonatomic, readonly) UIColor *color_theme;

/// 文本内容颜色
@property(class, nonatomic, readonly) UIColor *color_text;

/// 标题颜色
@property(class, nonatomic, readonly) UIColor *color_title;

/// 分割线颜色
@property(class, nonatomic, readonly) UIColor *color_splitLine;

/// 背景颜色
@property(class, nonatomic, readonly) UIColor *color_background;

/// 选项卡高亮颜色 例如确定、取消中的确定按钮颜色
@property(class, nonatomic, readonly) UIColor *color_option_highLight;

/// 选项卡高亮颜色 例如确定、取消中的取消按钮颜色
@property(class, nonatomic, readonly) UIColor *color_option_normal;

/// 阴影颜色
@property(class, nonatomic, readonly) UIColor *color_shadow;

/// 边框颜色 比如 layer.border
@property(class, nonatomic, readonly) UIColor *color_border;

@end

NS_ASSUME_NONNULL_END
