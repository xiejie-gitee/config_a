//
//  UIFont+Style.m
//  Objc
//
//  Author:     xiejie
//  Email:      632663019@qq.com
//  CreateDate: 2021/11/17
//
    

#import "UIFont+Style.h"

@implementation UIFont (Style)

+(UIFont *)fontSize:(float)size{
    return [UIFont fontWithName:@"PingFang-SC-Regular"size:size];
}
+(UIFont *)fontBoldSize:(float)size{
    return [UIFont fontWithName:@"PingFang-SC-Bold"size:size];

}
@end
