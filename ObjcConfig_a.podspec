Pod::Spec.new do |s|
    s.name         = "ObjcConfig_a"
    s.version      = "1.0.1"
    s.summary      = "custom XJTest"
    s.homepage     = "https://gitee.com/xiejie-gitee/config_a"
    s.license      = {:type=>"MIT",:file=>"LICENSE"}
 
    s.authors      = {"xiejie" => "632663019@qq.com"}
    s.platform     = :ios, "9.0"
    s.source       = {:git => "https://gitee.com/xiejie-gitee/config_a.git", :tag => s.version}
    s.source_files = "Objc/Classes/**/*.{h,m}"
    s.dependency "AFNetworking", "4.0"
    s.dependency "SDWebImage", "5.12.1"
    s.dependency "ObjcCategory"
    s.requires_arc = true
end

